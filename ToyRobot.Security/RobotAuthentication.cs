﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using ToyRobot.Core.Auth;
using ToyRobot.Core.Config;
using ToyRobot.Model.Auth;

namespace ToyRobot.Security
{
    public class RobotAuthentication : IRobotAuthentication
    {
        private readonly AuthConfig _authConfig;
        public RobotAuthentication(AuthConfig authConfig)
        {
            _authConfig = authConfig;
        }

        public AuthenticatedRobot AuthenticateRobot(string user, string secret)
        {
            if(_authConfig.User != user || _authConfig.Secret != secret)
            {
                return null;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_authConfig.Secret);
            var expires = DateTime.UtcNow.AddDays(1);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Expires = expires,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var jwt = tokenHandler.WriteToken(token);
            var unixExpiration = (int)expires.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

            return new AuthenticatedRobot(user, jwt, unixExpiration);
        }
    }
}
