﻿using Microsoft.Extensions.Caching.Memory;
using System;
using ToyRobot.Core.Movement;
using ToyRobot.Model.Compass;
using ToyRobot.Model.Movement;

namespace ToyRobot.Movement
{
    public class RobotMovement : IRobotMovement
    {
        private readonly IMemoryCache _memoryCache;

        public RobotMovement(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public RobotReport RobotSituation
        {
            get
            {
                if(_memoryCache.Get("RobotSituation") == null)
                {
                    throw new Exception("No place was set for the robot");
                }
                return (RobotReport)_memoryCache.Get("RobotSituation");
            }
        }

        public bool Place(RobotReport robotReport)
        {
            if(!robotReport.IsValid())
            {
                return false;
            }

            _memoryCache.Set("RobotSituation", robotReport);
            return true;
        }

        public bool Move()
        {
            var robotReport = RobotSituation;
            
            switch(robotReport.Face)
            {
                case EnumDirection.North:
                    robotReport.MoveNorth();
                    break;
                case EnumDirection.East:
                    robotReport.MoveEast();
                    break;
                case EnumDirection.South:
                    robotReport.MoveSouth();
                    break;
                case EnumDirection.West:
                    robotReport.MoveWest();
                    break;
            }

            return Place(robotReport);
        }

        public bool Left()
        {
            var robotReport = RobotSituation;

            robotReport.TurnLeft();

            return Place(robotReport);
        }

        public bool Right()
        {
            var robotReport = RobotSituation;

            robotReport.TurnRight();

            return Place(robotReport);
        }

        public RobotReport GetRobotReport()
        {
            return RobotSituation;
        }
    }
}
