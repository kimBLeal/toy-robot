Toy Robot Api

.Net core 2.1

*-----*

The robot will only move if provided the authorisation token.

*-----*

Run the project in Visual Studio. Should open the page https://localhost:5001/index.html or http://localhost:5000/index.html

It will open the Swagger documentation page.

To run on Swagger page:

Post Auth/Login:(the user and secret are in the appsettings config file)
user: toy-robot
secret: welcome2TOYROBOT

Then try the Robot api calls

*-----*

Postman collection link: https://www.getpostman.com/collections/cb1fedd1fa415b64ad1d

To run postman collections, first run the auth/login function and then the robot functions.

*-----*

For Sentry log errors:
https://sentry.io
user: toyrobot2019@gmail.com
password: welcome2TOYROBOT

