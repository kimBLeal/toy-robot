﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToyRobot.Model.Auth
{
    public class AuthenticatedRobot
    {
        public AuthenticatedRobot(string name, string accessToken, int expires)
        {
            Name = name;
            AccessToken = accessToken;
            Expires = expires;
        }

        public string Name { get; protected set; }
        public string AccessToken { get; protected set; }
        public int Expires { get; protected set; }
    }
}
