﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using ToyRobot.Model.Compass;

namespace ToyRobot.Model.Movement
{
    public class RobotReport
    {
        public RobotReport(int row, int column, string direction)
        {
            Place = new Tuple<int, int>(row, column);
            Face = (EnumDirection)Enum.Parse(typeof(EnumDirection), direction);
        }

        public Tuple<int, int> Place { get; protected set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public EnumDirection Face { get; protected set; }

        public void MoveNorth()
        {
            var newMove = Place.Item1 + 1;

            if (newMove >= 0 && newMove <= 4)
            {
                Place = new Tuple<int, int>(newMove, Place.Item2);
            }
        }

        public void MoveEast()
        {
            var newMove = Place.Item2 + 1;

            if (newMove >= 0 && newMove <= 4)
            {
                Place = new Tuple<int, int>(Place.Item1, newMove);
            }
        }

        public void MoveSouth()
        {
            var newMove = Place.Item1 - 1;

            if (newMove >= 0 && newMove <= 4)
            {
                Place = new Tuple<int, int>(newMove, Place.Item2);
            }
        }

        public void MoveWest()
        {
            var newMove = Place.Item2 - 1;

            if (newMove >= 0 && newMove <= 4)
            {
                Place = new Tuple<int, int>(Place.Item1, newMove);
            }
        }

        public void TurnLeft()
        {
            switch(Face)
            {
                case EnumDirection.North:
                    Face = EnumDirection.West;
                    break;
                case EnumDirection.East:
                    Face = EnumDirection.North;
                    break;
                case EnumDirection.South:
                    Face = EnumDirection.East;
                    break;
                case EnumDirection.West:
                    Face = EnumDirection.South;
                    break;
            }
        }

        public void TurnRight()
        {
            switch (Face)
            {
                case EnumDirection.North:
                    Face = EnumDirection.East;
                    break;
                case EnumDirection.East:
                    Face = EnumDirection.South;
                    break;
                case EnumDirection.South:
                    Face = EnumDirection.West;
                    break;
                case EnumDirection.West:
                    Face = EnumDirection.North;
                    break;
            }
        }

        public bool IsValid()
        {
            if (!Enum.IsDefined(typeof(EnumDirection), Face))
            {
                return false;
            }

            if (Place.Item1 < 0 || Place.Item1 > 4 
                || Place.Item2 < 0 || Place.Item2 > 4)
            {
                return false;
            }

            return true;
        }
    }
}
