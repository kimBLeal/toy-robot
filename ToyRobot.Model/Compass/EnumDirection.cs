﻿namespace ToyRobot.Model.Compass
{
    public enum EnumDirection
    {
        North,
        East,
        South,
        West
    }
}
