﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ToyRobot.Core.Auth;

namespace ToyRobot.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AuthController : ControllerBase
    {
        private readonly IRobotAuthentication _robotAuthentication;

        public AuthController(IRobotAuthentication robotAuthentication)
        {
            _robotAuthentication = robotAuthentication;
        }

        /// <summary>
		/// Gets the authentication token for the robot
		/// </summary>
		/// <param name="user"></param>
		/// <param name="secret"></param>
		/// <returns>
		/// JsonResult
		/// </returns>
        [HttpPost("login")]
        [AllowAnonymous]
        public IActionResult Login(string user, string secret)
        {
            if(string.IsNullOrEmpty(user) || string.IsNullOrEmpty(secret))
            {
                return BadRequest();
            }

            var authenticatedRobot = _robotAuthentication.AuthenticateRobot(user, secret);

            if(authenticatedRobot != null)
            {
                return Ok(authenticatedRobot);
            }

            return Unauthorized();
        }
    }
}