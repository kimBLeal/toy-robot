﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ToyRobot.Core.Movement;
using ToyRobot.Model.Movement;

namespace ToyRobot.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RobotController : ControllerBase
    {
        private readonly IRobotMovement _robotMovement;

        public RobotController(IRobotMovement robotMovement)
        {
            _robotMovement = robotMovement;
        }

        /// <summary>
        /// Place the robot in a space
        /// </summary>
        [HttpPost("place")]
        public IActionResult Place(int row, int column, string direction)
        {
            RobotReport robotReport = new RobotReport(row, column, direction);

            if(!_robotMovement.Place(robotReport))
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Move the robot one step forward
        /// </summary>
        [HttpPost("move")]
        public IActionResult Move()
        {
            if (!_robotMovement.Move())
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Face the robot to the left side
        /// </summary>
        [HttpPost("left")]
        public IActionResult Left()
        {
            if (!_robotMovement.Left())
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Face the robot to the right side
        /// </summary>
        [HttpPost("right")]
        public IActionResult Right()
        {
            if (!_robotMovement.Right())
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Get the current situation of the robot
        /// </summary>
        /// <returns>
        /// RobotReport
        /// </returns>
        [HttpGet("report")]
        public IActionResult Report()
        {
            var robotReport = _robotMovement.GetRobotReport();

            if(robotReport == null)
            {
                return BadRequest();
            }

            return Ok(robotReport);
        }
    }
}