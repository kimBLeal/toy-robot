﻿namespace LimeIntel.Core.Sentry
{
	public class SentryConfig
	{
		public string Dsn { get; set; }
        public string Environment { get; set; }
    }
}
