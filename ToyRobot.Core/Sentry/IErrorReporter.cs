﻿using System;
using System.Threading.Tasks;

namespace LimeIntel.Core.Sentry
{
	public interface IErrorReporter
	{
		Task CaptureAsync(Exception exception);
		Task CaptureAsync(string message);
	}
}
