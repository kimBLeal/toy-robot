﻿using ToyRobot.Model.Auth;

namespace ToyRobot.Core.Auth
{
    public interface IRobotAuthentication
    {
        AuthenticatedRobot AuthenticateRobot(string user, string secret);
    }
}
