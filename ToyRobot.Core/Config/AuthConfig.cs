﻿namespace ToyRobot.Core.Config
{
    public class AuthConfig
    {
        public string User { get; set; }
        public string Secret { get; set; }
    }
}
