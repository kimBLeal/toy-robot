﻿using ToyRobot.Model.Movement;

namespace ToyRobot.Core.Movement
{
    public interface IRobotMovement
    {
        bool Place(RobotReport robotReport);
        bool Move();
        bool Left();
        bool Right();
        RobotReport GetRobotReport();
    }
}
