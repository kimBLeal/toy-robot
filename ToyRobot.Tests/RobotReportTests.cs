using FluentAssertions;
using Microsoft.Extensions.Caching.Memory;
using Moq;
using NUnit.Framework;
using ToyRobot.Core.Movement;
using ToyRobot.Model.Compass;
using ToyRobot.Model.Movement;
using ToyRobot.Movement;

namespace Tests
{
    [TestFixture]
    public class RobotReportTests
    {
        [TestCase(0, 0, "North")]
        [TestCase(1, 0, "South")]
        [TestCase(2, 0, "East")]
        [TestCase(2, 3, "North")]
        [TestCase(4, 4, "West")]
        [TestCase(1, 2, "South")]
        [TestCase(4, 3, "East")]
        [TestCase(1, 1, "East")]
        public void It_should_return_true_when_place_is_valid(int row, int column, string direction)
        {
            var robotReport = new RobotReport(row, column, direction);

            robotReport.IsValid().Should().BeTrue();
        }

        [TestCase(5, 0, "North")]
        [TestCase(2, 5, "North")]
        [TestCase(4, 10, "West")]
        [TestCase(1, 6, "South")]
        [TestCase(9, 1, "East")]
        public void It_should_return_false_when_place_is_invalid(int row, int column, string direction)
        {
            var robotReport = new RobotReport(row, column, direction);

            robotReport.IsValid().Should().BeFalse();
        }

        public void It_should_move_one_step_north()
        {
            var robotReport = new RobotReport(0, 0, "North");
            robotReport.MoveNorth();

            robotReport.IsValid().Should().BeTrue();
            robotReport.Place.Item1.Should().Be(1);
            robotReport.Place.Item2.Should().Be(0);
            robotReport.Face.Should().Be(EnumDirection.North);
        }

        public void It_should_move_one_step_east()
        {
            var robotReport = new RobotReport(0, 0, "North");

            robotReport.TurnRight();
            robotReport.MoveEast();

            robotReport.IsValid().Should().BeTrue();
            robotReport.Place.Item1.Should().Be(0);
            robotReport.Place.Item2.Should().Be(1);
            robotReport.Face.Should().Be(EnumDirection.East);
        }

        public void It_should_not_move()
        {
            var robotReport = new RobotReport(0, 0, "North");

            robotReport.TurnLeft();
            robotReport.MoveWest();

            robotReport.IsValid().Should().BeTrue();
            robotReport.Place.Item1.Should().Be(0);
            robotReport.Place.Item2.Should().Be(0);
            robotReport.Face.Should().Be(EnumDirection.East);
        }
    }
}